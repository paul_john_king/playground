Unquoted

>>>
This is the header

* This is the first line
and this is a continuation line starting from the left margin

*   This is the second line, with no preceding blank line,
and this is a continuation starting without an indent

*   This is the third line, with a preceding blank line.

This is the footer
>>>

Unquoted

Options
-------

*   `-b <barney>`, `--bar <barney>`

    Bar `<barney>`, where
    
    *   `<barney>` is a Rubble, and
    
    *   `<barney>` lives in Bedrock.
    
    ----
    *   Required: No.
    *   Default: "Barney".
    ----
    
*   `-f <fred>`, `--foo <fred>`

    Foo `<fred>`, where
    
    *   `<fred>` is a Flintstone, and
    
    *   `<fred>` lives in Bedrock.
    
    ----
    *   Required: Yes.
    ----

These are not very useful options.

*   Create a directory `/backup` with
    *   user `mysql`,
    *   group `mysql`, and
    *   mode `drwxdr-xr-x`.

Links
-----
    
*   Link to https://arstechnica.com/ -- works.

*   [Link to Ars Technica (oneline)][ars] -- works.

*   [Link to Ars Technica (new line between text and target)]
    [ars] -- does not work!

*   [Link to Ars Technica
    (new line in text)][ars] -- works.

*   [Link to Ars Technica (new line in target)][
    ars] -- works.

[ars]: https://arstechnica.com/

Tables
------

Simple table

| key | value |
| --- | ----- |
| one | eins  |
| two | zwei  |

Complex table

| key | value                           |
| --- | ------------------------------- |
| one | this is the first of two lines  |
|     | this is the second of two lines |
| two | this is the first of two lines  |
|     | this is the second of two lines |